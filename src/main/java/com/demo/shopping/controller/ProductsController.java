package com.demo.shopping.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shopping")
public class ProductsController {
	
	
	@RequestMapping("/hello")
	public String hello() {
		return "hello";
	}

}
