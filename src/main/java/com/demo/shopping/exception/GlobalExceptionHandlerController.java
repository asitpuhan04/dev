package com.demo.shopping.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.demo.shopping.constants.AppConstants;

@ControllerAdvice
public class GlobalExceptionHandlerController {
	
	
	@ExceptionHandler(value = ProductNotFoundException.class)
	public ResponseEntity<String> handleCourseNotFoundException() {
		return new ResponseEntity<>(AppConstants.PRODUCT_NOT_FOUND,HttpStatus.NOT_FOUND);
	}

}
