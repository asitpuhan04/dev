package com.demo.shopping.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="product")
public class Product implements Serializable {
	
	@Id
	private long productId;
	
	private String productName;
	
	private int quantity;
	
	
	
	
	

}
