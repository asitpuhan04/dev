package com.demo.shopping.repository;

import org.springframework.data.repository.CrudRepository;

import com.demo.shopping.entity.Product;

public interface ProductsRepository extends CrudRepository<Product, Long> {

}
