package com.demo.shopping.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.shopping.entity.User;
import com.demo.shopping.repository.LoginRepository;

@Service
public class LoginService {
	
	@Autowired
	private LoginRepository loginRepository;
	
	
	public Optional<User> loginValidation(String email, String password) {
		return loginRepository.findStudentByEmailAndPasswordParams(email, password);

	}

}
